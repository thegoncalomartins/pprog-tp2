/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Interfaces;

/**
 *
 * @author Gonçalo Martins
 */
public interface ServicoPrecoMedio {
    public abstract float obterPrecoMedioPorPessoa();
}
