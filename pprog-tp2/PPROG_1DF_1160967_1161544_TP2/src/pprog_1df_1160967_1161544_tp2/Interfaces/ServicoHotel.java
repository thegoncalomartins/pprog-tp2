/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Interfaces;

/**
 *
 * @author Gonçalo Martins
 */
public interface ServicoHotel extends ServicoAlojamento {

    /**
     * String que representa uma estrela.
     */
    public static final String UMA_ESTRELA = "*";

    /**
     * String que representa duas estrelas.
     */
    public static final String DUAS_ESTRELAS = "**";

    /**
     * String que representa três estrelas.
     */
    public static final String TRES_ESTRELAS = "***";

    /**
     * String que representa quatro estrelas.
     */
    public static final String QUATRO_ESTRELAS = "****";

    /**
     * String que representa cinco estrelas.
     */
    public static final String CINCO_ESTRELAS = "*****";

    /**
     * String que representa a categoria por omissão (uma estrela).
     */
    public static final String CATEGORIA_POR_OMISSAO = UMA_ESTRELA;

    /**
     * Método que verifica se o Hotel tem ou não serviço de guia.
     * @return true se tiver, false caso contrário.
     */
    public abstract boolean validarServicoGuia();

    /**
     * Método que verifica se o Hotel tem ou não serviço de spa.
     * @return true se tiver, false caso contrário.
     */
    public abstract boolean validarServicoSPA();
}
