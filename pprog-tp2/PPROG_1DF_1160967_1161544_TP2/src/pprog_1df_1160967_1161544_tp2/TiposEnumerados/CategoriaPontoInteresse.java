/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.TiposEnumerados;

/**
 *
 * @author Gonçalo Martins
 */
public enum CategoriaPontoInteresse {
    
    /**
     * As categorias. 
     */
    MONUMENTO_CLASSICO { @Override public String toString() { return "Monumento Clássico"; } },
    MONUMENTO_CONTEMPORANEO { @Override public String toString() { return "Monumento Contemporâneo"; } },
    PRAÇA { @Override public String toString() { return "Praça"; } },
    OUTRO { @Override public String toString() { return "Outro"; } },

}
