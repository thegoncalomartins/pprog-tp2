/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.TiposEnumerados;

/**
 *
 * @author Gonçalo Martins
 */
public enum TipoComida {
    
    /**
     * Os tipos de comida.
     */
    COZINHA_TRADICIONAL_PORTUGUESA { @Override public String toString() { return "Cozinha Tradicional Portuguesa"; } },
    COZINHA_ITALIANA { @Override public String toString() { return "Cozinha Italiana"; } },
    COZINHA_CHINESA { @Override public String toString() { return "Cozinha Chinesa"; } },
    OUTRA { @Override public String toString() { return "Outra"; } },

   }
