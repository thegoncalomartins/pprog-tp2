/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

/**
 *
 * @author Gonçalo Martins
 */
public class Hostel extends EntidadeAlojamento {
    
    /**
     * O horário de receção do hostel.
     */
    private String horarioRececao;
    
    /**
     * O horário de receção por omissão.
     */
    private final String HORARIO_RECECAO_OMISSAO = "9h às 23h";

    /**
     * Construtor completo, recebe todos os atributos por parâmetro.
     * @param nome o nome do hostel
     * @param endereco o endereço do hostel
     * @param NIF o NIF do hostel
     * @param hasTransfer disponibilização de serviço de transfer
     * @param horarioRececao o horário de receção do hostel
     */
    public Hostel(String nome, String endereco, int NIF, boolean hasTransfer, String horarioRececao) {
        super(nome, endereco, NIF, hasTransfer);
        this.horarioRececao = horarioRececao;
    }

    /**
     * Construtor vazio.
     */
    public Hostel() {
        super();
        horarioRececao = HORARIO_RECECAO_OMISSAO;
    }
    
    /**
     * Construtor de cópia
     * @param outroHostel outro Hostel
     */
    public Hostel(Hostel outroHostel) {
        super(outroHostel);
        horarioRececao = outroHostel.horarioRececao;
    }

    /**
     * @return o horário de receção do Hostel
     */
    public String getHorarioRececao() {
        return horarioRececao;
    }

    /**
     * @param horarioRececao o horário de receção a definir
     */
    public void setHorarioRececao(String horarioRececao) {
        this.horarioRececao = horarioRececao;
    }

    @Override
    public String toString() {
        String transf;
        if (super.validarServicoTransfer()) {
            transf = "SIM";
        } else {
            transf = "NÃO";
        }
        return String.format("%n### HOSTEL ###%nNOME: %s%nNIF: %d%nENDEREÇO: %s%nHORÁRIO DE RECEÇÃO: %s%nTRANSFER: %s%n",
                ((Entidade)this).getNome(),
                ((EntidadeAlojamento)this).getNIF(),
                ((Entidade)this).getEndereco(),
                horarioRececao, 
                transf);
    }

    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((Hostel)obj).horarioRececao.equals(this.horarioRececao);
    }
   
}
