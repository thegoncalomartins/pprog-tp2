/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import pprog_1df_1160967_1161544_tp2.TiposEnumerados.CategoriaPontoInteresse;

/**
 *
 * @author Gonçalo Martins
 */
public class PontoInteresse extends Entidade {
    
    /**
     * A categoria do ponto de interesse.
     */
    private CategoriaPontoInteresse cat;
    
    /**
     * A categoria por omissão.
     */
    private final CategoriaPontoInteresse CAT_OMISSAO = CategoriaPontoInteresse.OUTRO; // por omissão deverá ser "Outro" (enunciado)
    
    /**
     * Construtor completo, recebe todos os atributos por parâmetro.
     * @param nome o nome do ponto de interesse
     * @param endereco o endereço do ponto de interesse
     * @param cat a categoria do ponto de interesse
     */
    public PontoInteresse(String nome, String endereco, CategoriaPontoInteresse cat) {
        super(nome, endereco);
        this.cat = cat;
    }
    
    /**
     * Construtor vazio.
     */
    public PontoInteresse() {
        super();
        cat = CAT_OMISSAO;
    }
    
    /**
     * Construtor de cópia
     * @param outroPI outro Ponto de Interesse
     */
    public PontoInteresse(PontoInteresse outroPI) {
        super(outroPI);
        this.cat = outroPI.cat;
    }

    /**
     * @return a categoria do ponto de interesse
     */
    public CategoriaPontoInteresse getCat() {
        return cat;
    }

    /**
     * @param cat categoria do ponto de interesse a definir
     */
    public void setCat(CategoriaPontoInteresse cat) {
        this.cat = cat;
    }
    
    @Override
    public String toString() {
        return String.format("%n### PONTO DE INTERESSE ###%nNOME: %s%nENDEREÇO: %s%nCATEGORIA: %s%n", super.getNome(), super.getEndereco(), cat);
    }

    
    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((PontoInteresse)obj).cat.equals(this.cat);
    }
    
}
