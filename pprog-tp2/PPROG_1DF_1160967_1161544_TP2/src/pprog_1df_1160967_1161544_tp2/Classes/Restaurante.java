/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import pprog_1df_1160967_1161544_tp2.Interfaces.ServicoPrecoMedio;
import pprog_1df_1160967_1161544_tp2.TiposEnumerados.TipoComida;

/**
 *
 * @author Gonçalo Martins
 */
public class Restaurante extends Entidade implements ServicoPrecoMedio {

    /**
     * O NIF do restaurante.
     */
    private int NIF;

    /**
     * O preço médio por pessoa.
     */
    private float precoMedioPessoa;

    /**
     * O tipo de comida servida pelo restaurante.
     */
    private TipoComida tipoComida;

    /**
     * O NIF por omissão.
     */
    private final int NIF_OMISSAO = 000000000;

    /**
     * O preço médio por pessoa por omissão.
     */
    private final float PRECO_MEDIO_PESSOA_OMISSAO = 0f;

    /**
     * O tipo de comida por omissão.
     */
    private final TipoComida TIPO_COMIDA_OMISSAO = TipoComida.COZINHA_TRADICIONAL_PORTUGUESA; // por omissão, deve ser considerada "Cozinha Tradicional Portuguesa" (enunciado)

    /**
     * Construtor completo, recebe todos os atributos por parâmetro.
     *
     * @param nome o nome do restaurante
     * @param endereco o endereço do restaurante
     * @param NIF o NIF do restaurante
     * @param tipoComida o tipo de comida servida pelo restaurante
     * @param precoMedioPessoa o preço médio por pessoa
     */
    public Restaurante(String nome, String endereco, int NIF, TipoComida tipoComida, float precoMedioPessoa) {
        super(nome, endereco);
        this.NIF = NIF;
        this.tipoComida = tipoComida;
        this.precoMedioPessoa = precoMedioPessoa;
    }

    /**
     * Construtor vazio.
     */
    public Restaurante() {
        super();
        NIF = NIF_OMISSAO;
        precoMedioPessoa = PRECO_MEDIO_PESSOA_OMISSAO;
        tipoComida = TIPO_COMIDA_OMISSAO;
    }

    /**
     * Construtor de cópia
     *
     * @param outroRestaurante outro Restaurante
     */
    public Restaurante(Restaurante outroRestaurante) {
        super(outroRestaurante);
        this.NIF = outroRestaurante.NIF;
        this.precoMedioPessoa = outroRestaurante.precoMedioPessoa;
        this.tipoComida = outroRestaurante.tipoComida;
    }

    /**
     * @return o NIF do Restaurante
     */
    public int getNIF() {
        return NIF;
    }

    /**
     * @return o tipo de comida servida
     */
    public TipoComida getTipoComida() {
        return tipoComida;
    }

    /**
     * @param NIF o NIF a definir
     */
    public void setNIF(int NIF) {
        this.NIF = NIF;
    }

    /**
     * @param precoMedioPessoa o preço médio por pessoa a definir (e.g. 12.5€)
     */
    public void setPrecoMedioPessoa(float precoMedioPessoa) {
        this.precoMedioPessoa = precoMedioPessoa;
    }

    /**
     * @param tipoComida o tipo de comida a definir
     */
    public void setTipoComida(TipoComida tipoComida) {
        this.tipoComida = tipoComida;
    }

    /**
     * Descrição do objeto Restaurante
     *
     * @return descrição atual do restaurante
     */
    @Override
    public String toString() {
        return String.format("%n### RESTAURANTE ###%nNOME: %s%nNIF: %d%nENDEREÇO: %s%nTIPO DE COMIDA: %s%nPREÇO MÉDIO POR PESSOA: %s%n",
                super.getNome(),
                NIF,
                super.getEndereco(),
                tipoComida,
                precoMedioPessoa);
    }

    /**
     * Método que obtém o preço médio por pessoa do restaurante
     *
     * @return o preço médio por pessoa
     */
    @Override
    public float obterPrecoMedioPorPessoa() {
        return precoMedioPessoa;
    }

    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((Restaurante) obj).NIF == this.NIF && ((Restaurante) obj).precoMedioPessoa == this.precoMedioPessoa && ((Restaurante) obj).tipoComida.equals(this.tipoComida);
    }

}
