/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import pprog_1df_1160967_1161544_tp2.Interfaces.ServicoAlojamento;

/**
 *
 * @author Gonçalo Martins
 */
public abstract class EntidadeAlojamento extends Entidade implements ServicoAlojamento {
    
    /**
     * O NIF da entidade de alojamento.
     */
    private int NIF;
    
    /**
     * Booleano que indica se a entidade de alojamento disponibiliza ou não serviço de transfer.
     */
    private boolean hasTransfer;
    
    /**
     * Disponibilização de serviço de transfer por omissão.
     */
    private final boolean HAS_TRANSFER_OMISSAO = ServicoAlojamento.SERVICO_POR_OMISSAO;
    
    /**
     * O NIF por omissão.
     */
    private final int NIF_OMISSAO = 000000000;
    
    /**
     * Construtor completo, recebe todos os atributos por parâmetro.
     * @param nome o nome da entidade de alojamento
     * @param endereco o endereço da entidade de alojamento
     * @param NIF o NIF da entidade de alojamento
     * @param hasTransfer disponibilização de serviço de transfer
     */
    public EntidadeAlojamento(String nome, String endereco, int NIF, boolean hasTransfer) {
        super(nome, endereco);
        this.hasTransfer = hasTransfer;
        this.NIF = NIF;
    }
    
    /**
     * Construtor vazio.
     */
    public EntidadeAlojamento() {
        super();
        hasTransfer = HAS_TRANSFER_OMISSAO;
        NIF = NIF_OMISSAO;
    }
    
    /**
     * Construtor de cópia
     * @param outraEntidade outra entidade de alojamento
     */
    public EntidadeAlojamento(EntidadeAlojamento outraEntidade) {
        super(outraEntidade);
        this.hasTransfer = outraEntidade.hasTransfer;
        this.NIF = outraEntidade.NIF;
    }

    /**
     * @return o NIF da entidade de alojamento
     */
    public int getNIF() {
        return NIF;
    }

    /**
     * @param NIF o NIF a definir
     */
    public void setNIF(int NIF) {
        this.NIF = NIF;
    }

    /**
     * @param hasTransfer booleano que indica se a entidade de alojamento tem ou não serviço de transfer
     */
    public void setHasTransfer(boolean hasTransfer) {
        this.hasTransfer = hasTransfer;
    }
    
    @Override
    public abstract String toString();
    
    /**
     * Método booleano que indica se a entidade de alojamento tem ou não tem serviço de transfer.
     * @return true se tiver, false caso contrário.
     */
    @Override
    public boolean validarServicoTransfer() {
        return hasTransfer;
    }
    
    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((EntidadeAlojamento)obj).NIF == this.NIF && ((EntidadeAlojamento)obj).hasTransfer == this.hasTransfer;
    }
}
