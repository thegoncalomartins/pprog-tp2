/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import pprog_1df_1160967_1161544_tp2.TiposEnumerados.*;
import pprog_1df_1160967_1161544_tp2.Interfaces.*;


/**
 *
 * @author Gonçalo Martins
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // critério de ordenação
        Comparator<Entidade> criterio = new Comparator<Entidade>() {


            @Override
            public int compare(Entidade e1, Entidade e2) {
                int c = e1.getClass().getSimpleName().compareTo(e2.getClass().getSimpleName());
                
                if (c == 0) {
                    return e1.getNome().compareTo(e2.getNome());
                }
                return c;
            }

        };
        
        
        

        BomServico bs = new BomServico("BeYou", "229610319", 284632981);
        
        Entidade hotel1 = new Hotel("Hotel do Templo", "Rua da Laranjeira", 223456987, true, ServicoHotel.QUATRO_ESTRELAS, false, true);
        bs.inserirEntidade(hotel1);
        
        Entidade hotel2 = new Hotel("Hotel do Elevador", "Rua Eng. Pinto", 198457256, false, ServicoHotel.TRES_ESTRELAS, true, false);
        bs.inserirEntidade(hotel2);
        
        Entidade hotel3 = new Hotel("Hotel do Parque", "Rua dos Milagres", 216543873, true, ServicoHotel.CINCO_ESTRELAS, true, true);
        bs.inserirEntidade(hotel3);
        
        Entidade hostel1 = new Hostel("Boa Vista", "Avenida Dr. Carlos Felgueiras", 199999999, false, "8h30 às 20h30");
        bs.inserirEntidade(hostel1);
        
        Entidade hostel2 = new Hostel("Oásis", "Rua Grande", 188888888, false, "9h às 20h");
        bs.inserirEntidade(hostel2);
        
        Entidade restaurante1 = new Restaurante("TakeIt", "Rua dos Catramilos", 198765412, TipoComida.COZINHA_ITALIANA, 8.50f);
        bs.inserirEntidade(restaurante1);
        
        Entidade restaurante2 = new Restaurante();
        bs.inserirEntidade(restaurante2);
        
        Entidade restaurante3 = new Restaurante();
        bs.inserirEntidade(restaurante3);
        
        Entidade pi1 = new PontoInteresse();
        bs.inserirEntidade(pi1);
        
        Entidade pi2 = new PontoInteresse();
        bs.inserirEntidade(pi2);
        
        Entidade pi3 = new PontoInteresse();
        bs.inserirEntidade(pi3);
        
        Entidade pi4 = new PontoInteresse();
        bs.inserirEntidade(pi4);
        
        listarPrecosMediosPessoa(utilizadorEscolheTipoComida(), bs);
        
        listarHoteisComTransfer(utilizadorEscolheCategoriaHotel(), bs);
        
        listarPontosInteresse(utilizadorIndicaGrauSatisfacao(), bs);
        
        listarEntidadesEmpresa(bs, criterio);
        
        
    }

    /**
     * Escreve para o ecrã os preços médios por pessoa dos restaurantes
     * @param tipo tipo de comida servida pelo restaurante
     * @param bs empresa BomServiço
     */
    public static void listarPrecosMediosPessoa(TipoComida tipo, BomServico bs) {
        System.out.println("\n### Listagem dos Preços Médios por Pessoa ###\n");
        for(Entidade e: bs.getEntidades()) {
            if (e instanceof Restaurante && ((Restaurante)e).getTipoComida().toString().equals(tipo.toString())) {
               
                System.out.println(e.getNome());
                System.out.println(((Restaurante)e).obterPrecoMedioPorPessoa() + "€" + "\n");
            }
        }
        System.out.println();
    }
    
    /**
     * Escreve para o ecrã todos os hotéis com serviço de transfer 
     * @param categoria categoria do hotel
     * @param bs empresa BomServiço
     */
    public static void listarHoteisComTransfer(String categoria, BomServico bs) {
        System.out.println("\n### Listagem dos Hotéis com Transfer ###\n");
        for(Entidade e: bs.getEntidades()) {
            if (e instanceof Hotel && ((Hotel)e).validarServicoTransfer() && ((Hotel)e).getCategoria().equals(categoria)) {
                System.out.println(((Hotel)e).toString());
            }
        }
        System.out.println();
    }
    
    /**
     * Escreve para o ecrã todos os pontos de interesse com um grau da satisfação igual ou superior ao fornecido pelo utilizador
     * @param grauSatisfacao grau de satisfação fornecido pelo utilizador
     * @param bs empresa BomServiço
     */
    public static void listarPontosInteresse(int grauSatisfacao, BomServico bs) {
        System.out.println("\n### Listagem dos Pontos de Interesse ###\n");
        for(Entidade e: bs.getEntidades()) {
            if (e instanceof PontoInteresse && e.obterAvaliacao()>= grauSatisfacao) {
                System.out.println(((PontoInteresse)e).toString());
            }
        }
        System.out.println();
    }
    
    /**
     * Escreve para o ecrã todas as entidades da Empresa BomServiço
     * @param bs empresa BomServiço
     * @param criterio critério de ordenação
     */
    public static void listarEntidadesEmpresa(BomServico bs, Comparator<Entidade> criterio) {
        System.out.println("### Listagem das Entidades ###\n");
        List<Entidade> ets = new ArrayList<>(bs.getEntidades()); // cria uma cópia do ArrayList das entidades da instância bs, de BomServico
        Collections.sort(ets, criterio);
        for(Entidade e: ets) {
            System.out.printf("%n### %s ###"
                    + "%nNOME: %s"
                    + "%nENDEREÇO: %s%n", e.getClass().getSimpleName(), e.getNome(), e.getEndereco());
        }
        System.out.println();
    }

    /**
     * Método que permite ao utilizador escolher o tipo de comida pretendida.
     * @return o tipo de comida
     */
    public static TipoComida utilizadorEscolheTipoComida() {
        System.out.print("Insira o tipo de comida pretendida: ");
        Scanner in = new Scanner(System.in);
        String tipo = in.nextLine();
        
        for (TipoComida t : TipoComida.values()) {
            if (t.toString().equalsIgnoreCase(tipo)) {
                return t;
            }
        }
        return TipoComida.values()[3];
    }
    
    /**
     * Método que permite ao utilizador escolher a categoria pretendida de um hotel
     * @return string que representa a categoria do hotel
     */
    public static String utilizadorEscolheCategoriaHotel() {
        System.out.print("Insira a categoria do Hotel: ");
        
        Scanner in = new Scanner(System.in);
        
        String categoria = in.nextLine();
        
        if (categoria.equals(ServicoHotel.UMA_ESTRELA)) {
            return ServicoHotel.UMA_ESTRELA;
        }
        
        if (categoria.equals(ServicoHotel.DUAS_ESTRELAS)) {
            return ServicoHotel.DUAS_ESTRELAS;
        }
        
        if (categoria.equals(ServicoHotel.TRES_ESTRELAS)) {
            return ServicoHotel.TRES_ESTRELAS;
        }
        
        if (categoria.equals(ServicoHotel.QUATRO_ESTRELAS)) {
            return ServicoHotel.QUATRO_ESTRELAS;
        }
        
        return ServicoHotel.CINCO_ESTRELAS;
        
    }
    
    /**
     * Método que permite ao utilizador indicar o grau de satisfação pretendido.
     * @return inteiro que representa o grau de satisfação.
     */
    public static int utilizadorIndicaGrauSatisfacao() {
        System.out.print("Insira o grau de satisfação pretendido (1 a 5): ");
        Scanner in = new Scanner(System.in);
        
        int grau = in.nextInt();
        return grau;
    }
}
