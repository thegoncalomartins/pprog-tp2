/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import pprog_1df_1160967_1161544_tp2.Interfaces.ServicoHotel;

/**
 *
 * @author Gonçalo Martins
 */
public class Hotel extends EntidadeAlojamento implements ServicoHotel {
    
    /**
     * A categoria do hotel.
     */
    private String categoria;

    /**
     * Booleano que indica se o hotel disponibiliza ou não serviço de guia.
     */
    private boolean hasGuia;
    
    /**
     * Booleano que indica se o hotel disponibiliza ou não serviço de spa.
     */
    private boolean hasSPA;
    
    /**
     * A categoria por omissão.
     */
    private final String CATEGORIA_OMISSAO = ServicoHotel.CATEGORIA_POR_OMISSAO;
    
    /**
     * Disponibilização de serviço de guia por omissão.
     */
    private final boolean HAS_GUIA_OMISSAO = false;
    
    /**
     * Disponibilização de serviço de spa por omissão.
     */
    private final boolean HAS_SPA_OMISSAO = false;

    
    /**
     * Construtor completo, recebe todos os atributos por parâmetro.
     * @param nome o nome do hotel
     * @param endereco o endereço do hotel
     * @param NIF o NIF do hotel
     * @param hasTransfer disponibilização de serviço de transfer
     * @param categoria a categoria do hotel
     * @param hasGuia disponibilização de serviço de guia
     * @param hasSPA disponibilização de serviço de spa
     */
    public Hotel(String nome, String endereco, int NIF, boolean hasTransfer, String categoria, boolean hasGuia, boolean hasSPA) {
        super(nome, endereco, NIF,  hasTransfer);
        this.categoria = categoria;
        this.hasGuia = hasGuia;
        this.hasSPA = hasSPA;
        
    }
    
    /**
     * Construtor vazio.
     */
    public Hotel() {
        super();
        categoria = CATEGORIA_OMISSAO;
        hasGuia = HAS_GUIA_OMISSAO;
        hasSPA = HAS_SPA_OMISSAO;
    }
    
    /**
     * Construtor de cópia
     * @param outroHotel outro Hotel
     */
    public Hotel(Hotel outroHotel) {
        super(outroHotel);
        this.categoria = outroHotel.categoria;
        this.hasGuia = outroHotel.hasGuia;
        this.hasSPA = outroHotel.hasSPA;
    }

    /**
     * @return a categoria do Hotel
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @param categoria a categoria a definir
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * @param hasGuia booleano que indica se o hotel tem ou não guia
     */
    public void setHasGuia(boolean hasGuia) {
        this.hasGuia = hasGuia;
    }

    /**
     * @param hasSPA booleano que indica se o hotel tem ou não spa
     */
    public void setHasSPA(boolean hasSPA) {
        this.hasSPA = hasSPA;
    }

    /**
     * Método que verifica se o hotel tem serviço de guia
     * @return true se tiver, false caso contrário.
     */
    @Override
    public boolean validarServicoGuia() {
        return hasGuia;
    }

    /**
     * Método que verifica se o hotel tem serviço de spa
     * @return true se tiver, false caso contrário.
     */
    @Override
    public boolean validarServicoSPA() {
        return hasSPA;
    }

    @Override
    public String toString() {
        
        String guia, spa, transf;
        
        if (hasGuia) {
            guia = "SIM";
        } else {
            guia = "NÃO";
        }
        
        if (hasSPA) {
            spa = "SIM";
        } else {
            spa = "NÃO";
        }
        
        if (super.validarServicoTransfer()) {
            transf = "SIM";
        } else {
            transf = "NÃO";
        }
        
        return String.format("%n### HOTEL ###%nNOME: %s%nNIF: %d%nENDEREÇO: %s%nCATEGORIA: %s%nGUIA: %s%nSPA: %s%nTRANSFER: %s%n",
                ((Entidade)this).getNome(),
                ((EntidadeAlojamento)this).getNIF(), 
                ((Entidade)this).getEndereco(), 
                categoria, 
                guia, 
                spa, 
                transf);
    }

    
    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((Hotel)obj).categoria.equals(this.categoria) && ((Hotel)obj).hasGuia == this.hasGuia && ((Hotel)obj).hasSPA == this.hasSPA;
    }

}
