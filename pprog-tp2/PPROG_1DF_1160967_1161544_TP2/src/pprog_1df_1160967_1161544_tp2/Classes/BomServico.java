/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *
 * @author Gonçalo Martins
 */
public class BomServico {

    /**
     * Nome da empresa BomServiço.
     */
    private String nome;

    /**
     * Telefone da empresa.
     */
    private String telefone;

    /**
     * Nº Contribuinte da empresa.
     */
    private int NIF;

    /**
     * ArrayList que contém as entidades da empresa BomServiço.
     */
    private List<Entidade> entidades = new ArrayList<>();; // não sei se colocamos como constante de instância ou como variável de instância...

    /**
     * Nome por omissão da empresa.
     */
    private final String NOME_OMISSAO = "Sem Nome";
    
    /**
     * Telefone da empresa por omissão.
     */
    private final String TELEFONE_OMISSAO = "000000000";
    
    /**
     * Nº Contribuinte da empresa por omissão.
     */
    private final int NIF_OMISSAO = 000000000;

    /**
     * Construtor completo
     *
     * @param nome da empresa BomServiço
     * @param telefone telefone da empresa
     * @param NIF Nº de Contribuinte da empresa
     */
    public BomServico(String nome, String telefone, int NIF) {
        this.nome = nome;
        this.telefone = telefone;
        this.NIF = NIF;
    }

    /**
     * Construtor vazio
     */
    public BomServico() {
        nome = NOME_OMISSAO;
        telefone = TELEFONE_OMISSAO;
        NIF = NIF_OMISSAO;
       
    }

    /**
     * Construtor de cópia
     *
     * @param outraEmpresa outra empresa BomServiço
     */
    public BomServico(BomServico outraEmpresa) {
        this.nome = outraEmpresa.nome;
        this.telefone = outraEmpresa.telefone;
        this.NIF = outraEmpresa.NIF;
        this.entidades = outraEmpresa.entidades;
    }

    /**
     * @return o nome da Empresa BomServiço
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return o nº de telefone da Empresa BomServiço
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @return o NIF da Empresa BomServiço
     */
    public int getNIF() {
        return NIF;
    }

    
    /**
     * @return o ArrayList das entidades da empresa BomServiço
     */
    public List<Entidade> getEntidades() {
        return Collections.unmodifiableList(entidades); // disponibiliza o ArrayList apenas para leitura, não permite fazer alterações
        // excerto de código retirado de http://stackoverflow.com/questions/2516778/java-how-to-have-an-arraylist-as-instance-variable-of-an-object
        // do utilizador Alexander Pogrebnyak
    }
    
    
    /**
     * @param nome o nome a definir
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param telefone o nº de telefone a definir
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @param NIF o NIF a definir
     */
    public void setNIF(int NIF) {
        this.NIF = NIF;
    }

    /**
     * @return descrição geral da empresa BomServiço
     */
    @Override
    public String toString() {
        return String.format("### EMPRESA BOMSERVIÇO ###NOME: %s%nTELEFONE: %s%nNIF: %s%n", nome, telefone, NIF);
    }
    
    /**
     * Permite agregar uma entidade à empresa BomServiço
     * @param e entidade a ser agregada
     */
    public void inserirEntidade(Entidade e) {
        entidades.add(e);
    }
    
    /**
     * Devolve a avaliação da entidade recebida por parâmetro. Devolve -1 se a entidade não existir.
     * @param e entidade.
     * @return avaliação da entidade.
     */
    public float obterAvaliacao(Entidade e) {
        
        for (Entidade ents : entidades) {
            
            if (ents.equals(e)) {
                return e.obterAvaliacao();
            }
        }
        return -1;
    }
 
    /**
     * Atualiza a avaliação da entidade recebida por parâmetro. Retorna true se a entidade existir e false se a entidade não existir.
     * @param val valor para atualizar.
     * @param e entidade.
     * @return boolean true em caso de sucesso, false caso contrário.
     */
    public boolean atualizarAvaliacao(int val, Entidade e) {
        
        for (Entidade ents : entidades) {
            
            if (ents.equals(e)) {
                e.atualizarAvaliacao(val);
                return true;
            }
        }
        return false;
    }

    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        BomServico bs = (BomServico) obj;
        return nome.equals(bs.nome) && telefone.equals(bs.telefone) && NIF == bs.NIF && entidades == bs.entidades;
    }

}
