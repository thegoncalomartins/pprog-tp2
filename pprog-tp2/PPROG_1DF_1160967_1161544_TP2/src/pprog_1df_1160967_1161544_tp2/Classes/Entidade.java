/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_1df_1160967_1161544_tp2.Classes;

import java.util.ArrayList;
import java.util.List;
import pprog_1df_1160967_1161544_tp2.Interfaces.ServicoAvaliacao;

/**
 *
 * @author Gonçalo Martins
 */
public abstract class Entidade implements ServicoAvaliacao {

    /**
     * O nome da entidade.
     */
    private String nome;

    /**
     * O endereço da entidade.
     */
    private String endereco;

    /**
     * ArrayList que contém as avaliações dadas à entidade.
     */
    private List<Integer> avaliacoes = new ArrayList<>();

    /**
     * O nome por omissão.
     */
    private final String NOME_OMISSAO = "Sem nome";

    /**
     * O endereço por omissão.
     */
    private final String ENDERECO_OMISSAO = "Sem endereço";

    /**
     * Construtor completo, recebe todos os atributos por parâmetro.
     *
     * @param nome o nome da entidade
     * @param endereco o endereço da entidade
     */
    public Entidade(String nome, String endereco) {
        this.nome = nome;
        this.endereco = endereco;
    }

    /**
     * Construtor vazio.
     */
    public Entidade() {
        nome = NOME_OMISSAO;
        endereco = ENDERECO_OMISSAO;
    }

    /**
     * Construtor de cópia
     * @param outraEntidade outra entidade
     */
    public Entidade(Entidade outraEntidade) {
        this.nome = outraEntidade.nome;
        this.endereco = outraEntidade.endereco;
        this.avaliacoes = outraEntidade.avaliacoes;
    }

    /**
     * @return o nome da entidade
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return o endereço da entidade
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param nome o nome a definir
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param endereco o endereço a definir
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * Descrição geral da entidade.
     *
     * @return String com informação geral sobre a entidade.
     */
    @Override
    public abstract String toString();

    /**
     * Método que obtém a média das avaliações de uma entidade.
     *
     * @return a média das avaliações dadas pelos utilizadores.
     */
    @Override
    public float obterAvaliacao() {
        
        if (avaliacoes.size() == 0) {
            return 0f;
        }
        
        int soma = 0;
        for (Integer i : avaliacoes) {
            soma += i;
        }
        return ((float) soma / avaliacoes.size());
    }

    /**
     * Método para avaliar uma entidade.
     *
     * @param val o valor da avaliação (entre 1 e 5)
     */
    @Override
    public void atualizarAvaliacao(int val) {
        avaliacoes.add(val);
    }

    /**
     * @return true se os objetos forem o mesmo ou tiverem os atributos todos iguais, false caso contrário.
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Entidade outraEntidade = (Entidade) obj;
        return this.nome.equals(outraEntidade.nome) && this.endereco.equals(outraEntidade.endereco) && this.avaliacoes == outraEntidade.avaliacoes;
    }

    

}
